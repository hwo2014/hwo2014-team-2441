using System;
using System.IO;
using System.Net.Sockets;
using Newtonsoft.Json;
using System.Collections.Generic;


public class Bot
{
    List<CarPosition> carPositions;
    double speed = 0;
    double acceleration = 0;
    CarPosition lastPosition = null;
    double lastSpeed = 0;
    MyCar myCar;
    int lastTick = -1;
    Track track;
    bool switchMsgSent = false;
    bool switching = false;
    CarPosition myPosition;
    double slipAngle;
    int lap;
    double throttle = 0;
    Piece currTrackPiece;
    List<Piece> nextCurve;
    int nextCurveLaneIndex;
    int switchLaneTo;
    double curveForceLimit = 0.42;
    bool hasCrashed = false;
    List<Piece> longestStraight;
    bool turboReady = false;
    TurboData turboData;
    Track.TrackSegment currSwitchSegment = null;
    bool overtaking = false;

    DataLogger dataLogger = new DataLogger();
    double basePower = 0;
    double baseFriction = 0;

    public static void Main(string[] args)
    {

        string host = args[0];
        int port = int.Parse(args[1]);
        string botName = args[2];
        string botKey = args[3];
        string trackName = (args.Length > 4) ? args[4] : null;
        int numPlayers = (args.Length > 5) ? int.Parse(args[5]) : 1;
        SendMsg join;


        Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);
        if (trackName != null)
        {
            Console.WriteLine("Track name: " + trackName);
            join = new JoinRace(botName, botKey, trackName, null, numPlayers);
        }
        else
        {
            join = new Join(botName, botKey);
        }

        using (TcpClient client = new TcpClient(host, port))
        {
            NetworkStream stream = client.GetStream();
            StreamReader reader = new StreamReader(stream);
            StreamWriter writer = new StreamWriter(stream);
            writer.AutoFlush = true;

            new Bot(reader, writer, join);
        }
    }

    private List<CarPosition> GetCarsInNextTwoPiece(int lane)
    {
        List<CarPosition> result = new List<CarPosition>();
        foreach (CarPosition car in carPositions)
        {

            if (myPosition.PiecePosition.PieceIndex == car.PiecePosition.PieceIndex && car.PiecePosition.lane.EndLaneIndex == lane)
            {
                Console.WriteLine("Found car in myBlock");
                result.Add(car);
            }
            if ((myPosition.PiecePosition.PieceIndex + 1) % track.Pieces.Count == car.PiecePosition.PieceIndex && car.PiecePosition.lane.EndLaneIndex == lane)
            {
                Console.WriteLine("Found car in next block");
                result.Add(car);
            }
        }
        return result;
    }

    //Returnes the list of cars in the Lane in the segmern
    private List<CarPosition> GetCarsInSegment(Track.TrackSegment segment, int lane)
    {
        List<CarPosition> result = new List<CarPosition>();
        foreach (CarPosition car in carPositions)
        {
            Console.WriteLine("TEsting for car in segment");
            if (segment.ContainsPiece(car.PiecePosition.PieceIndex) == true)
            {
                Console.WriteLine("Found car in segment");
                result.Add(car);
            }
        }
        return result;
    }

    private bool IsNextTwoPiecesOccupied()
    {
        int lane = myPosition.PiecePosition.lane.StartLaneIndex;
        foreach (CarPosition carPosition in carPositions) {
            bool maybeBlocking = false;
            if (carPosition.Id.Name == myCar.Name) {
                continue;
            }
            if (carPosition.PiecePosition.PieceIndex == myPosition.PiecePosition.PieceIndex)
            {
                maybeBlocking = true;
                Console.WriteLine("On same block");
            }
            if (carPosition.PiecePosition.PieceIndex == (myPosition.PiecePosition.PieceIndex + 1) % track.Pieces.Count)
            {
                maybeBlocking = true;
                Console.WriteLine("Not on very next block");
            }
            if (lane == carPosition.PiecePosition.lane.StartLaneIndex && maybeBlocking == true)
            {
                Console.WriteLine("Blocking found next too");
                return true;
            }
        }
        return false;
    }

    //Returnes true if a competitor is in the lane in the segment
    private bool IsSegmentOccupied(Track.TrackSegment segment, int lane)
    {
        Console.WriteLine("Checking for blocking: num cars " + carPositions.Count);
        foreach (CarPosition carPosition in carPositions)
        {
            bool carNear = false;
            if (carPosition.Id.Name == myCar.Name)
            {
                Console.WriteLine("My car");
                continue;
            }
            if (segment.ContainsPiece(carPosition.PiecePosition.PieceIndex) == true)
            {
                Console.WriteLine("In the spot");
                carNear = true;
            }
            if (lane == carPosition.PiecePosition.lane.StartLaneIndex && carNear == true)
            {
                Console.WriteLine("Blocking found");
                return true;
            }
        }
        return false;
    }

    private void CalculateTelemetry(int newTick)
    {
        //Standard loading
        myPosition = carPositions.Find(x => x.Id.Name == myCar.Name);
        slipAngle = myPosition.Angle;
        lap = myPosition.Lap;
        currTrackPiece = track.Pieces[myPosition.PiecePosition.PieceIndex];

        //First message is parsed here
        if (newTick == 0)
        {
            lastPosition = myPosition;
            speed = 0;
            lastSpeed = 0;
        }
        //Second message is parsed here
        else if (lastTick == 0)
        {
            //Calculate new value
            double distance = track.GetDistanceInLane(lastPosition, myPosition);
            int time = newTick - lastTick;
            speed = distance / ((time != 0) ? time : 1);
            //Store new values for next call
            lastSpeed = speed;
            lastPosition = myPosition;

        }
        //The rest of the messages are parsed here
        else
        {

            //Calculate new values
            double distance = track.GetDistanceInLane(lastPosition, myPosition);
            int time = newTick - lastTick;
            speed = distance / ((time != 0) ? time : 1);
            acceleration = (speed - lastSpeed) / ((time != 0) ? time : 1);
            //Calculate more accurate base power value;
            if (newTick == 4)
            {
                basePower = acceleration - (lastSpeed * baseFriction);
                Console.WriteLine("New Base Power " + basePower);
            }
            //Store new values for next call
            lastSpeed = speed;
            lastPosition = myPosition;
            //Calculate braking force
            if (newTick == 3)
            {
                baseFriction = acceleration / speed;
                Console.WriteLine("Base friction: " + baseFriction);
            }
            //Calc initial power
            if (newTick == 2)
            {
                basePower = acceleration;
                Console.WriteLine("Base power: " + basePower);
            }

        }


        //Update variables

        lastTick = newTick;

        //Check if the current trackpieces has a switch and sets the switching flag
        int currPosIndex = myPosition.PiecePosition.PieceIndex;
        Piece currPiece = track.Pieces[currPosIndex];
        if (currPiece.HasSwitch == true)
        {
            switching = true;
        }
        //Resest the switchMsgSent flag, when a switch has been passed
        if (switching == true && currPiece.HasSwitch == false)
        {
            switchMsgSent = false;
            switching = false;
        }

        //Calculate next curve if necessary
        if (nextCurve == null)
        {
            nextCurve = track.GetNextCurve(currTrackPiece);
            //Console.WriteLine("Next curve was indefined, next curve starts at" + track.Pieces.IndexOf(nextCurve[0]));
        }
        else if (nextCurve.Contains(currTrackPiece))
        {
            nextCurve = track.GetNextCurve(currTrackPiece);
            //Console.WriteLine("In new curve, next curve starts at" + track.Pieces.IndexOf(nextCurve[0]));
        }

        //Calculates which lane the next curve will be in?
        int nextSwitch = GetNextSwitchIndex();
        //Finds whichever comes first, switch or next curve
        int index = currPosIndex;
        while (index != nextSwitch && index != track.Pieces.IndexOf(nextCurve[0]))
        {
            if (index == nextSwitch)
            {
                nextCurveLaneIndex = switchLaneTo;
            }
            else if (index == track.Pieces.IndexOf(nextCurve[0]))
            {
                nextCurveLaneIndex = myPosition.PiecePosition.lane.EndLaneIndex;
            }
            index = (index + 1) % track.Pieces.Count;
        }

        //Sets current track segment
        foreach (Track.TrackSegment segment in track.SwitchSegments)
        {
            if (segment.Pieces.Contains(track.Pieces[currPosIndex]))
            {
                currSwitchSegment = segment;
            }
        }
    }

    private int GetNextSwitchIndex()
    {
        int index = myPosition.PiecePosition.PieceIndex;
        int nextSwitchIndex;
        bool found = false;
        while (found == false)
        {
            index = (index + 1) % track.Pieces.Count;
            found = track.Pieces[index].HasSwitch;

        }
        nextSwitchIndex = index;
        return nextSwitchIndex;
    }

    //Decides which action to take, when a carPositions message has been sent
    private void Respond()
    {


        //if first 2 tick, send throttle!
        if (lastTick <= 3)
        {
            send(new ThrottleTick(throttle, lastTick));
            Console.WriteLine("Init throttle tick");
            return;
        }
        int currPieceIndex = myPosition.PiecePosition.PieceIndex;
        
        //Index of next switch track piece. Does not look at the current piece;   
        int nextSwitch = GetNextSwitchIndex();
        //Next switch segment
        Track.TrackSegment nextSwitchSegment = track.SwitchSegments.Find(s => s.StartIndex == (nextSwitch + 1) % track.Pieces.Count);

        //If we are overtaking we need to know when we are in the straight
        if (overtaking == true)
        {
            
            bool useTurbo = false;
            //If we are on the switch 
            if (track.Pieces[myPosition.PiecePosition.PieceIndex].HasSwitch == true)
            {
                Console.WriteLine("On switch");
                //If the piece following the switch is on the straight
                if (nextSwitchSegment.StartStraight == (myPosition.PiecePosition.PieceIndex + 1) % track.Pieces.Count)
                {
                    useTurbo =  true;
                }
            }
            //else simply check if we are on the straight of the segment
            else if (currSwitchSegment.PieceIsInStraight(track.Pieces[myPosition.PiecePosition.PieceIndex]))
            {
                useTurbo = true;
            }
            Console.WriteLine("Straight index: " + currSwitchSegment.StartStraight + " myPos: " + myPosition.PiecePosition.PieceIndex);
            //Check we are in new lane
            foreach (CarPosition car in carPositions)
            {
                if (car.Id.Name == myCar.Name) continue;
                if (car.PiecePosition.lane.EndLaneIndex == myPosition.PiecePosition.lane.EndLaneIndex)
                {
                    Console.WriteLine("Still in same lane");
                    useTurbo = false;
                }
            }
            //Send the message
            if (useTurbo == true)
            {
                Console.WriteLine("OVERTAKING!");
                send(new TurboTick("TURBO Message!", lastTick));
                turboReady = false;
                overtaking = false;
                return;
            }
        }
        //Decides if a switch response needs to be sent
        if (switchMsgSent == false && currSwitchSegment.EndIndex == currPieceIndex)
        {
            

            int currLaneIndex = myPosition.PiecePosition.lane.EndLaneIndex;
            int toLaneIndex = track.GetShortestSwitchSegment(nextSwitch, currLaneIndex);
            //Console.WriteLine("Current lane index: " + currLaneIndex);
            //Is the shortest lane blocked?
            
            if (IsSegmentOccupied(nextSwitchSegment, toLaneIndex) == true || IsNextTwoPiecesOccupied())
            {
                Console.WriteLine("Found blocked segment");
                //Is there space to overtake
                bool overTakeSpace = (nextSwitchSegment.StraightLength >= 200);

                //How far is the opposition?
                List<CarPosition> carsInSector = GetCarsInSegment(nextSwitchSegment, toLaneIndex);
                carsInSector.AddRange(GetCarsInNextTwoPiece(toLaneIndex));
                double nearest = double.MaxValue;
                PiecePosition myPosCopy;
                foreach (CarPosition car in carsInSector)
                {
                    if (car.Id.Name == myCar.Name) continue;
                    myPosCopy = new PiecePosition(myPosition.PiecePosition.PieceIndex, myPosition.PiecePosition.InPieceDistance, car.PiecePosition.lane , lap);
                    double distance = track.GetDistanceInLane(myPosCopy, car.PiecePosition);
                    nearest = (distance < nearest) ? distance : nearest;
                }
                Console.WriteLine("Opposition distance: " + nearest + " overtake space: " + overTakeSpace);
                //Is the opposition near enough for overtake, do we have boost and is there space?
                if (nearest <= 150 && overTakeSpace == true && turboReady == true)
                {
                    Console.WriteLine("Choose new lane");
                    double shortest = double.MaxValue;
                    double second = double.MaxValue;
                    int secondIndex = 5;
                    for (int i = 0; i < nextSwitchSegment.LaneDistances.Count; i++)
                    {
                        double d = nextSwitchSegment.LaneDistances[i];
                        if (i == toLaneIndex)
                        {
                            Console.WriteLine("Curr Lane index : " + i);
                            continue;
                        }
                        if (d < shortest)
                        {
                            second = shortest;
                            shortest = d;
                            secondIndex = i;
                        }
                        else if (d < second)
                        {
                            second = d;
                            secondIndex = i;
                        }
                    }
                    //Check if free route was found
                    if (secondIndex != 5) 
                    {
                        Console.WriteLine("New lane found: " + toLaneIndex);
                        toLaneIndex = secondIndex;
                        overtaking = true;
                    }
                    else
                    {
                        Console.WriteLine("Aborting overtake");
                        //Give up and take the shortest blocked path
                    }
                }

            }

            //Translate the decision to a message
            switchLaneTo = toLaneIndex;
            if (toLaneIndex > currLaneIndex)
            {
                //turnRight
                send(new SwitchLaneTick("Right", lastTick));
                switchMsgSent = true;
                //Console.WriteLine("Switching Right");
            }
            else if (toLaneIndex < currLaneIndex)
            {
                //TurnLeft
                send(new SwitchLaneTick("Left", lastTick));
                switchMsgSent = true;
                //Console.WriteLine("Switching Left");
            }
            else
            {
                //Do nothing
                send(new ThrottleTick(throttle, lastTick));
                //Console.WriteLine("Message sent: Throttle " + throttle);
                switchMsgSent = true;
                //Console.WriteLine("Doing Nothing");
            }

        }
        else if (turboReady == true)
        {
            if (myPosition.PiecePosition.PieceIndex == track.Pieces.IndexOf(longestStraight[0]))
            {
                send(new TurboTick("TURBO Message!", lastTick));
                turboReady = false;
            }
            else
            {
                send(new ThrottleTick(throttle, lastTick));
            }
        }
        else
        {
            //Console.WriteLine("Message sent: Throttle " + throttle);
            send(new ThrottleTick(throttle, lastTick));
        }
    }
    private double SpeedTested = 0;
    private bool braking = false;
    private void TestSpeeds()
    {
        if (SpeedTested == dataLogger.NumColumns) dataLogger.AddColumn("\"Speed\",\"Acceleration\"");

        dataLogger.AddToColumn("\"" + speed + "\",\"" + acceleration + "\"", (int)SpeedTested);
        if (braking == true)
        {
            if (speed < 0.001)
            {
                braking = false;
                throttle = 1;
                SpeedTested++;
                Console.WriteLine("Accelerating to: " + SpeedTested);
            }
            else
            {
                throttle = 0;
            }
        }
        else if (speed > SpeedTested)
        {
            braking = true;
            throttle = 0;
            Console.WriteLine("Reached speed: " + SpeedTested + " Braking");

        }
        else
        {
            throttle = 1;

        }

    }

    private void SetThrottle()
    {
        double maxSpeed = double.MaxValue; //Arbitrarily high
        //Calculate max speed for current piece;
        if (currTrackPiece.GetType() == typeof(Curve))
        {
            double maxPieceSpeed = currTrackPiece.CalcMaxSpeed(curveForceLimit, myPosition.PiecePosition.lane.EndLaneIndex);
            maxSpeed = (maxSpeed > maxPieceSpeed) ? maxPieceSpeed : maxSpeed;
        }

        //calculate max speed for next corner
        double nextCornerMaxSpeed = nextCurve[0].CalcMaxSpeed(curveForceLimit, nextCurveLaneIndex);
        //Console.WriteLine("Next corner max speed: " + nextCornerMaxSpeed);
        //calculate space needed to brake
        double brakingSpace = GetDistanceForBraking(speed, nextCornerMaxSpeed);

        //calculate next position after 1 tick
        int currLane = myPosition.PiecePosition.lane.EndLaneIndex;
        //S0 + v^2 - v0^2 / 2 * a; 
        double nextPosition = myPosition.PiecePosition.InPieceDistance + GetDistanceForAcceleration(speed, PredictedSpeed(throttle, speed), basePower * throttle + baseFriction + speed);
        //Create a pieceposition object for next position
        PiecePosition nextPiecePosition;
        if (nextPosition > currTrackPiece.CalcLength(currLane))
        {
            PiecePosition.Lane tempLane = new PiecePosition.Lane(myPosition.PiecePosition.lane.StartLaneIndex, currLane);
            nextPiecePosition = new PiecePosition((myPosition.PiecePosition.PieceIndex + 1) % track.Pieces.Count, nextPosition - currTrackPiece.CalcLength(currLane), tempLane, lap);
        }
        else
        {
            nextPiecePosition = new PiecePosition(myPosition.PiecePosition.PieceIndex, nextPosition, myPosition.PiecePosition.lane, lap);
        }

        //Calculate distance to next corner;
        PiecePosition nextCurvePiece = new PiecePosition(track.Pieces.IndexOf(nextCurve[0]), 0, new PiecePosition.Lane(nextCurveLaneIndex, nextCurveLaneIndex), lap);
        double distanceToNextCurve = track.GetDistanceInLane(nextPiecePosition, nextCurvePiece); //Calculates distance to curve in one tick
        //Console.WriteLine("Next step index: " + nextPiecePosition.PieceIndex + " Next piece positiom: " + nextPiecePosition.InPieceDistance);
        //Console.WriteLine("Distance to curve: " + distanceToNextCurve + " Braking distance: " + brakingSpace);

        //Do we need to slow down for corner now?

        bool brakeForCorner = brakingSpace >= distanceToNextCurve && brakingSpace > 0; //Distance to curve attemts at noticing if we should brake on step before it is too late

        //set throttle
        //Throttle up or down
        if (lastTick == 2)
        {
            throttle = 0;
            Console.WriteLine("Testing Brake power. Speed: " + speed);
        }
        else if (brakeForCorner)
        {
            throttle = 0;
            Console.WriteLine("Braking before corner. Speed: " + speed);
        }
        /*else if (Math.Abs(slipAngle) > 40)
        {
            throttle = 0;
            Console.WriteLine("Braking because of slip angle");
        }*/
        else if (speed > maxSpeed)
        {
            throttle = 0;
            Console.WriteLine("Braking to stay on track. Speed: " + speed + " Desired speed: " + maxSpeed);
        }
        else if (speed < maxSpeed)
        {
            throttle = 1;
            Console.WriteLine("Accelerating, Speed: " + speed + " Desired speed: " + maxSpeed);
        }

    }

    private double PredictedSpeed(double throttle)
    {
        return PredictedSpeed(1, throttle);
    }

    private double PredictedSpeed(int ticks, double throttle)
    {
        return PredictedSpeed(ticks, throttle, speed);
    }
    protected double PredictedSpeed(double throttle, double speed)
    {
        return PredictedSpeed(1, throttle, speed);
    }
    protected double PredictedSpeed(int ticks, double throttle, double speed)
    {
        double newSpeed = speed;
        for (int i = 0; i < ticks; i++)
        {
            newSpeed = newSpeed + (throttle * basePower + (newSpeed * baseFriction));
        }
        return newSpeed;
    }

    private double GetDistanceForBraking(double initialSpeed, double finalSpeed)
    {
        //Console.WriteLine("Initial speed: " + initialSpeed + " Final speed: " + finalSpeed);
        if (initialSpeed < finalSpeed)
        {
            return 0;
        }
        int step = 0;

        //Calculate breaking distance in steps
        //step 0
        double speedAtStep = initialSpeed;
        //step 1
        //Console.WriteLine("Speed at step 1: " + speedAtStep);
        step = 1;
        double speedAtLastStep = speedAtStep;
        speedAtStep = PredictedSpeed(0.0, speedAtStep);
        double distance = GetDistanceForAcceleration(speedAtLastStep, speedAtStep, speedAtLastStep * baseFriction);
        //step n
        while (speedAtStep >= finalSpeed)
        {
            step++;
            speedAtLastStep = speedAtStep;
            speedAtStep = PredictedSpeed(0.0, speedAtStep);
            distance += GetDistanceForAcceleration(speedAtLastStep, speedAtStep, speedAtLastStep * baseFriction);
            //Console.WriteLine("Step " + step + " Speed at step: " + speedAtStep + " Distance: " + distance);
        }


        return distance;
    }

    private double GetDistanceForAcceleration(double initialSpeed, double finalSpeed, double acceleration)
    {
        return ((finalSpeed * finalSpeed) - (initialSpeed * initialSpeed)) / (2 * acceleration);
    }

    private StreamWriter writer;

    Bot(StreamReader reader, StreamWriter writer, SendMsg join)
    {

        this.writer = writer;
        string line;

        send(join);

        while ((line = reader.ReadLine()) != null)
        {
            try
            {
                int newTick;
                MsgWrapper msg = JsonConvert.DeserializeObject<MsgWrapper>(line);
                GameTickData tick = JsonConvert.DeserializeObject<GameTickData>(line);
                newTick = tick.GameTick;
                switch (msg.msgType)
                {
                    case "carPositions":

                        //Update carpositions before you calculate telemetry
                        //Console.WriteLine("Predicted acceleration: " + (basePower * throttle + (speed * baseFriction)));
                        //Console.WriteLine("Tick: " + newTick);
                        carPositions = parsePosition(msg);
                        CalculateTelemetry(newTick);
                        //Console.WriteLine("Speed: " + speed);
                        //Console.WriteLine("Acceleration: " + acceleration);
                        if (hasCrashed == false)
                        {

                            SetThrottle();

                            //Console.WriteLine("Acceleration: " + acceleration);
                            //TestSpeeds();
                            Respond();

                        }
                        else
                        {
                            send(new Ping());
                        }

                        break;
                    case "join":
                        Console.WriteLine("Joined");
                        //send(new Ping());
                        break;
                    case "gameInit":
                        speed = 0;
                        acceleration = 0;
                        lastSpeed = 0;
                        switchMsgSent = false;
                        switching = false;
                        throttle = 0;
                        hasCrashed = false;
                        turboReady = false;
                        nextCurve = null;
                        Track.TrackSegment currSwitchSegment = null;
                        overtaking = false;

                        Console.WriteLine("Race init");
                        track = parseTrack(msg);

                        longestStraight = track.GetLongestStraight();
                        hasCrashed = false;
                        //send(new Ping());
                        break;
                    case "gameEnd":
                        Console.WriteLine("Race ended");
                        //send(new Ping());
                        break;
                    case "gameStart":
                        Console.WriteLine("Race starts");
                        hasCrashed = false;
                        SetThrottle();
                        Respond();
                        //send(new Ping());
                        break;
                    case "yourCar":
                        myCar = JsonConvert.DeserializeObject<MyCar>(msg.data.ToString());
                        //send(new Ping());
                        break;
                    case "crash":
                        Crash crashData = JsonConvert.DeserializeObject<Crash>(msg.data.ToString());
                        PrintCrashMsg(crashData);
                        break;
                    case "spawn":
                        Crash spawnData = JsonConvert.DeserializeObject<Crash>(msg.data.ToString());
                        SpawnRecieved(spawnData);
                        break;
                    case "turboAvailable":
                        turboData = JsonConvert.DeserializeObject<TurboData>(msg.data.ToString());
                        turboReady = true;
                        Console.WriteLine("Turbo ready!");
                        break;
                    case "turboStart":
                        CarId turboCarStart = JsonConvert.DeserializeObject<CarId>(msg.data.ToString());
                        if (turboCarStart.Name == myCar.Name)
                        {
                            basePower *= turboData.TurboFactor;
                            Console.WriteLine("Server recognized our turbo!");
                            Console.WriteLine("BasePower: " + basePower);
                            turboReady = false;

                        }
                        break;
                    case "turboEnd":
                        CarId turboCarEnd = JsonConvert.DeserializeObject<CarId>(msg.data.ToString());
                        if (turboCarEnd.Name == myCar.Name)
                        {
                            basePower /= turboData.TurboFactor;
                            Console.WriteLine("Our turbo ended :(");
                            Console.WriteLine("bassPower: " + basePower);
                        }
                        break;
                    default:
                        //send(new Ping());
                        break;
                }
                dataLogger.WriteData();
            }
            catch (Exception e)
            {

                Console.WriteLine("Something went very wrong.");
                Console.WriteLine(e);
                send(new Ping());
            }
            
        }

        

    }

    //The Data in spawn and crash messages are the same fomat therefore i use crash objects to store spawn data
    private void SpawnRecieved(Crash spawnMsg)
    {
        if (spawnMsg.Name == myCar.Name)
        {
            Console.WriteLine("We drive again");
            hasCrashed = false;
            turboReady = false;
            overtaking = false;
        }
    }

    private void PrintCrashMsg(Crash crashMsg)
    {
        if (crashMsg.Name == myCar.Name)
        {
            turboReady = false;
            Console.WriteLine("Fuck! We crashed");
            Console.WriteLine("Speed: " + speed + " Acceleration: " + acceleration + " Throttle: " + throttle + " Angle: " + slipAngle);
            Console.WriteLine("Piece Index: " + myPosition.PiecePosition.PieceIndex + " In piece distance: " + myPosition.PiecePosition.InPieceDistance);
            Console.WriteLine("Piece type: " + track.Pieces[myPosition.PiecePosition.PieceIndex].Name);
            Console.WriteLine("Centripetal force: " + CalcCentripetalForce());
            Console.WriteLine("################################################################################");
            hasCrashed = true;
        }
    }

    private List<CarPosition> parsePosition(MsgWrapper msg)
    {
        if (msg.msgType == "carPositions")
        {
            return JsonConvert.DeserializeObject<List<CarPosition>>(msg.data.ToString());
        }
        Console.WriteLine("ERROR: Message is not of type 'carPositions' but " + msg.msgType);
        return null;
    }

    private Track parseTrack(MsgWrapper msg)
    {
        //Race track;
        if (msg.msgType == "gameInit")
        {
            //track = JsonConvert.DeserializeObject<Race>(msg.data.ToString());
            RaceData race = JsonConvert.DeserializeObject<RaceData>(msg.data.ToString());


            foreach (Piece piece in race.Race.Track.Pieces)
            {
                //Console.WriteLine(piece.Name);
            }


            return race.Race.Track;
        }
        else
        {
            Console.WriteLine("ERROR: Parse track method called on " + msg.msgType + " Message");
            return null;
        }
    }

    private void send(SendMsg msg)
    {
        writer.WriteLine(msg.ToJson());
    }

    private double CalcCentripetalForce()
    {
        Piece currPiece = track.Pieces[myPosition.PiecePosition.PieceIndex];
        if (currPiece.GetType() == typeof(Curve))
        {
            Curve curve = currPiece as Curve;
            int currLane = myPosition.PiecePosition.lane.EndLaneIndex;
            double laneOffset = track.Lanes[currLane].DistanceFromCenter;
            //speed squared devided by radius;
            double force = (speed * speed) / curve.AdjustedRadius(laneOffset);
            return force;

        }
        return 0;
    }
}


class TurboData
{
    private double _turboDurationMiliseconds;
    private int _turboDurationTicks;
    private double _turboFactor;

    public TurboData(double turboDurationMiliseconds, int turboDurationTicks, double turboFactor)
    {
        this._turboDurationMiliseconds = turboDurationMiliseconds;
        this._turboDurationTicks = turboDurationTicks;
        this._turboFactor = turboFactor;
    }

    public double TurboDurationMiliseconds { get { return this._turboDurationMiliseconds; } }
    public int TurboDurationTicks { get { return this._turboDurationTicks; } }
    public double TurboFactor { get { return this._turboFactor; } }
}

class DataLogger
{
    private List<List<string>> _data;

    public DataLogger()
    {

        this._data = new List<List<string>>();
    }
    public List<string> AddColumn(string columnName)
    {
        List<string> column = new List<string>();
        column.Add(columnName);
        _data.Add(column);
        return column;

    }

    public void AddToColumn(string data, int column)
    {
        _data[column].Add(data);
    }
    public int NumColumns { get { return _data.Count; } }

    public void WriteData()
    {
        List<string> lines = new List<string>();
        for (int i = 0; i < LongestColumn(); i++)
        {
            lines.Add("");
        }
        foreach (List<string> column in _data)
        {
            for (int i = 0; i < LongestColumn(); i++)
            {
                //If data add it
                if (column.Count > i)
                {
                    lines[i] += column[i];
                }
                //else add empty
                else
                {
                    lines[i] += "\"\",\"\"";
                }
                //if column is not the last, add comma seperator
                if (_data.IndexOf(column) < _data.Count - 1)
                {
                    lines[i] += ",";
                }
            }
        }
        //write lines to file
       // StreamWriter outFile = new StreamWriter(".\\outData.csv");
        foreach (string s in lines)
        {
        //    outFile.WriteLine(s);
        }
        //outFile.Close();
    }
    private int LongestColumn()
    {
        int length = int.MinValue;
        foreach (List<string> l in _data)
        {
            length = (l.Count > length) ? l.Count : length;
        }
        return length;
    }
}

//Matches a Crash message
class Crash
{
    private string _name;
    private string _color;

    public Crash(string name, string color)
    {
        this._name = name;
        this._color = color;
    }

    public string Name { get { return this._name; } }
    public string Color { get { return this._color; } }

}

//Matches yourCar message
class MyCar
{
    string _name;
    string _color;

    public MyCar(string name, string color)
    {
        this._name = name;
        this._color = color;
    }

    public string Name { get { return this._name; } }
    public string Color { get { return this._color; } }
}

//Matches gametick from CarPosition message
class GameTickData
{
    private int _gameTick;

    public GameTickData(int gameTick)
    {
        this._gameTick = gameTick;

    }

    public int GameTick { get { return this._gameTick; } }
}

//Matches the gameInit Date package
class RaceData
{
    private Race _race;

    public RaceData(Race race)
    {
        this._race = race;
    }

    public Race Race { get { return this._race; } }
}

//Matches the carpositions piece data package
class PiecePosition
{
    public class Lane
    {
        private int _startLaneIndex;
        private int _endLaneIndex;

        public Lane(int startLaneIndex, int endLaneIndex)
        {
            this._startLaneIndex = startLaneIndex;
            this._endLaneIndex = endLaneIndex;
        }

        public int StartLaneIndex { get { return _startLaneIndex; } }
        public int EndLaneIndex { get { return _endLaneIndex; } }
    }
    private int _pieceIndex;
    private double _inPieceDistance;
    private PiecePosition.Lane _lane;
    private int _lap;

    public PiecePosition(int pieceIndex, double inPieceDistance, PiecePosition.Lane lane, int lap)
    {
        this._pieceIndex = pieceIndex;
        this._inPieceDistance = inPieceDistance;
        this._lane = lane;
        this._lap = lap;
    }

    public int PieceIndex { get { return this._pieceIndex; } }
    public double InPieceDistance { get { return this._inPieceDistance; } }
    public PiecePosition.Lane lane { get { return this._lane; } }
    public int Lap { get { return this._lap; } }

}

//Matches the carposition package
class CarPosition
{
    private CarId _id;
    private double _angle;
    private PiecePosition _piecePosition;
    private int _lap;

    public CarPosition(CarId id, double angle, PiecePosition piecePosition, int lap)
    {
        this._id = id;
        this._angle = angle;
        this._piecePosition = piecePosition;
        this._lap = lap;
    }

    public CarId Id { get { return this._id; } }
    public double Angle { get { return this._angle; } }
    public PiecePosition PiecePosition { get { return this._piecePosition; } }
    public int Lap { get { return this._lap; } }

}

//Matches the Race session part of the gameInit package
class RaceSession
{
    private int _laps;
    private int _maxTimeMs;
    private bool _quickRace;

    public RaceSession(int laps, int maxTimeMs, bool quickRace)
    {
        this._laps = laps;
        this._maxTimeMs = maxTimeMs;
        this._quickRace = quickRace;
    }

    public int Laps { get { return this._laps; } }
    public int MaxTimeMs { get { return this._maxTimeMs; } }
    public bool QuickRace { get { return this._quickRace; } }
}

//matches the race part of the gameInit message
class Race
{
    private Track _track;
    private List<Car> _cars;
    private RaceSession _raceSession;

    public Race(Track track, List<Car> cars, RaceSession raceSession)
    {
        this._track = track;
        this._cars = cars;
        this._raceSession = raceSession;
    }

    public Track Track { get { return this._track; } }
    public List<Car> Cars { get { return this._cars; } }
    public RaceSession RaceSession { get { return this._raceSession; } }
}
//Matches the carid part of the cars object of the gameInit message 
public class CarId
{
    private string name;
    private string key;
    private string color;

    [JsonConstructor]
    CarId(string name, string color)
        : this(name, color, null)
    {

    }

    CarId(string name, string color, string key)
    {
        this.name = name;
        this.color = color;
        this.key = key;
    }

    public string Name { get { return this.name; } }
    public string Key { get { return this.key; } }
    public string Color { get { return this.color; } }
}

//Matches the dimensions of the car message of gameInit
class Dimensions
{
    private double _length;
    private double _width;
    private double _guideFlagPosition;

    public Dimensions(double length, double width, double guideFlagPosition)
    {
        this._length = length;
        this._width = width;
        this._guideFlagPosition = guideFlagPosition;
    }

    public double Length { get { return this._length; } }
    public double Width { get { return this._width; } }
    public double GuideFlagPosition { get { return this._guideFlagPosition; } }
}

//Matches the cars of the gameInit message
class Car
{
    private CarId _id;
    private Dimensions _dimensions;

    public Car(CarId id, Dimensions dimensions)
    {
        this._id = id;
        this._dimensions = dimensions;
    }

    public CarId Id { get { return _id; } }
    public Dimensions Dimensions { get { return _dimensions; } }
}
//Matches the track part of the gameInit message
class Track
{
    public class TrackSegment
    {
        private List<Piece> _pieces;
        private List<Piece> _straight;
        private List<double> _laneDistances;
        private int _startIndex;
        private int _endIndex;
        private int _startStraight;
        private int _endStraight;
        private double _straightLength;

        public TrackSegment(List<Piece> segment, Track track)
        {
            this._pieces = segment;
            this._straight = CalcLongStraight(track.Pieces.IndexOf(segment[0]), track.Pieces.IndexOf(segment[segment.Count - 1]) , track);
            this._startIndex = track.Pieces.IndexOf(segment[0]);
            this._endIndex = track.Pieces.IndexOf(segment[segment.Count - 1]);
            if (Straight.Count > 0)
            {
                this._startStraight = track.Pieces.IndexOf(Straight[0]);
                this._endStraight = track.Pieces.IndexOf(Straight[Straight.Count - 1]);
            }
            else
            {
                this._startStraight = this._startIndex;
                this._endStraight = this._startIndex;
            }
            this._laneDistances = track.GetLanesDistances(segment);
            this._straightLength = GetStraightLength();
        }
        public TrackSegment(int startIndex, int endIndex, Track track)
        {
            this._pieces = track.GetTrackPieces(startIndex, endIndex);
            this._straight = CalcLongStraight(startIndex, endIndex, track);
            this._startIndex = startIndex;
            this._endIndex = endIndex;
            if (Straight.Count > 0)
            {
                this._startStraight = track.Pieces.IndexOf(Straight[0]);
                this._endStraight = track.Pieces.IndexOf(Straight[Straight.Count - 1]);
            }
            else
            {
                this._startStraight = startIndex;
                this._endStraight = startIndex;
            }
            this._laneDistances = track.GetLanesDistances(Pieces);
            this._straightLength = GetStraightLength();
        }

        private double GetStraightLength()
        {
            double length = 0;
            foreach (Straight p in Straight)
            {
                length += p.Length;
            }
            return length;
        }

        private List<Piece> CalcLongStraight(int startIndex, int endIndex, Track track)
        {
            return track.GetLongestStraight(startIndex, endIndex);
        }
        //Possible error
        public bool ContainsPiece(int index)
        {
            if (index >= StartIndex && index <= EndIndex)
            {
                return true;
            }
            return false;
        }
        public bool PieceIsInStraight(Piece piece)
        {
            return Straight.Contains(piece);
        }

        public List<Piece> Pieces { get { return this._pieces; } }
        public List<Piece> Straight { get { return this._straight; } }
        public List<double> LaneDistances { get { return this._laneDistances; } }
        public int StartIndex { get { return this._startIndex; } }
        public int EndIndex { get { return this._endIndex; } }
        public int StartStraight { get { return this._startStraight; } }
        public int EndStraight { get { return this._endStraight; } }
        public double StraightLength { get { return this._straightLength; } }
    }


    private string _id;
    private string _name;
    private List<Piece> _pieces;
    private List<Lane> _lanes;
    private List<PieceData> _piecesData;
    private List<TrackSegment> _switchSegments;

    public Track(string id, string name, List<PieceData> pieces, List<Lane> lanes)
    {
        this._id = id;
        this._name = name;
        this._piecesData = pieces;
        this._lanes = lanes;
        this._pieces = new List<Piece>();

        //Converts the general pieces to specific types
        foreach (PieceData piece in this._piecesData)
        {
            //If straight
            if (piece.Length != 0)
            {
                double length = piece.Length;
                bool hasSwitch = piece.HasSwitch;
                this._pieces.Add(new Straight(length, hasSwitch));

            }
            //If curve
            if (piece.Angle != 0)
            {
                double radius = piece.Radius;
                double angle = piece.Angle;
                bool hasSwitch = piece.HasSwitch;
                this._pieces.Add(new Curve(radius, angle, hasSwitch));

            }
        }

        this._switchSegments = new List<TrackSegment>();
        Piece firstSwitch = GetNextSwitch(0);
        int indexOfCurrP1 = (Pieces.IndexOf(firstSwitch) + 1) % Pieces.Count;
        Piece currSwitch = GetNextSwitch(indexOfCurrP1);
        int indexOfNext = Pieces.IndexOf(currSwitch);
        this._switchSegments.Add(new Track.TrackSegment(GetTrackPieces(indexOfCurrP1, indexOfNext), this));
        Console.WriteLine("P1: " + indexOfCurrP1 + " M1: " + indexOfNext);

        while (currSwitch != firstSwitch)
        {
            indexOfCurrP1 = (Pieces.IndexOf(currSwitch) + 1) % Pieces.Count;
            Piece nextSwitch = GetNextSwitch(indexOfCurrP1);
            indexOfNext = Pieces.IndexOf(nextSwitch);
            Console.WriteLine("Curr switch id: " + Pieces.IndexOf(currSwitch) + " next switch id: " + Pieces.IndexOf(nextSwitch));
            this._switchSegments.Add(new Track.TrackSegment(GetTrackPieces(indexOfCurrP1, indexOfNext), this));
            
            currSwitch = nextSwitch;
        }

    }

    //Finds the next switch piece
    public Piece GetNextSwitch(int index)
    {
        Piece currPiece = Pieces[index];
        while (currPiece.HasSwitch == false)
        {
            index = (index + 1) % Pieces.Count;
            currPiece = Pieces[index];
        }
        return currPiece;
    }

    //Calculates the length of all of the Pieces in a segment of track
    public List<double> GetLanesDistances(List<Piece> segment)
    {
        List<double> distances = new List<double>();
        foreach (Lane lane in Lanes)
        {
            distances.Add(0);
            foreach (Piece piece in segment)
            {
                distances[lane.Index] += piece.CalcLength(lane.DistanceFromCenter);
            }
        }
        return distances;
    }

    //calculate the length of a lane in a section of track
    public double GetDistanceInLane(List<Piece> segment, int lane)
    {
        double distance = 0;
        double offSet = Lanes[lane].DistanceFromCenter;
        foreach (Piece p in segment)
        {
            distance += p.CalcLength(offSet);
        }
        return distance;
    }

    //Find the longest straight in the entire track
    public List<Piece> GetLongestStraight()
    {
        return GetLongestStraight(0, Pieces.Count - 1);
    }
    public List<Piece> GetLongestStraight(PiecePosition start, PiecePosition end)
    {
        return GetLongestStraight(start.PieceIndex, end.PieceIndex);
    }
    //This one assumes that the indexes are the inedxes of the of pieces in the Track.Pieces list
    public List<Piece> GetLongestStraight(int startIndex, int endIndex)
    {
        List<List<Piece>> straights = new List<List<Piece>>();
        straights.Add(new List<Piece>());
        List<double> straightLengths = new List<double>();
        List<Piece> longestStraight = new List<Piece>();
        Console.WriteLine("Start index: " + startIndex + "End index: " + endIndex);
        //Adds all the straight to a list of straights
        int numStraights = 0;
        bool inCurve = false;
 
        for (int i = startIndex; i != endIndex ; i = (i + 1) % Pieces.Count)
        {
            Console.WriteLine("I: " +i + " NumStraights " + numStraights);
            Piece p = Pieces[i];
            Console.WriteLine("Type of piece:" + p.GetType());
            if (p.GetType() == typeof(Straight))
            {
                inCurve = false;
                straights[numStraights].Add(p);
            }
            else if (numStraights == straights.Count - 1 && inCurve == false)
            {
                if (straights[numStraights].Count != 0)
                {
                    numStraights++;
                    straights.Add(new List<Piece>());
                }
                inCurve = true;
            }
        }
        Piece last = Pieces[endIndex];
        Console.WriteLine("I: " + endIndex + " NumStraights " + numStraights);
        Console.WriteLine("Type of piece:" + last.GetType());
        if (last.GetType() == typeof(Straight))
            {
                inCurve = false;
                straights[numStraights].Add(last);
            }
            else if (numStraights == straights.Count - 1 && inCurve == false)
            {
                if (straights[numStraights].Count != 0)
                {
                    numStraights++;
                    straights.Add(new List<Piece>());
                }
                inCurve = true;
            }
        
        //Calc lengths of straights
        foreach (List<Piece> straight in straights)
        {
            straightLengths.Add(0);
            foreach (Piece piece in straight)
            {
                if (piece.GetType() == typeof(Straight))
                {

                    straightLengths[straights.IndexOf(straight)] += (piece as Straight).Length;
                }
            }
        }
        
        //find longest
        double longestStraightLength = 0;
        int longStraightIndex;
        foreach (double l in straightLengths)
        {
            //If straight is longer than the previous
            if (l > longestStraightLength)
            {
                longestStraightLength = l;
                longStraightIndex = straightLengths.IndexOf(l);
                longestStraight = straights[longStraightIndex];
            }
        }
        Console.WriteLine("We get here!");
        //If startSection seperates a straight
        if (straights[0].Count > 0)
        {
            Console.WriteLine("Long straights");
            List<Piece> lastStraight = straights[straights.Count - 1];
            //Console.WriteLine("Pieces: " + Pieces.Count + " Striaghts: " + straights.Count + " Straights 2: " + straights[0].Count);
            if (Pieces[0] == straights[0][0] && Pieces[Pieces.Count - 1] == lastStraight[lastStraight.Count - 1])
            {
                Console.WriteLine("You think?");
                //If it is the longest straight
                double length = straightLengths[0] + straightLengths[straightLengths.Count - 1];
                if (length > longestStraightLength)
                {
                    longestStraightLength = length;
                    longStraightIndex = straightLengths.Count - 1;
                    straights[longStraightIndex].AddRange(straights[0]);
                    longestStraight = straights[longStraightIndex];
                }
            }
            
        }
        Console.WriteLine("We get here?");
        return longestStraight;
    }



    //Gets the segment og track including the start piece, and excluding the end piece
    public List<Piece> GetTrackPieces(PiecePosition start, PiecePosition end)
    {
        return GetTrackPieces(start.PieceIndex, end.PieceIndex);
    }

    public List<Piece> GetTrackPieces(int startIndex, int endIndex)
    {
        List<Piece> result = new List<Piece>();
        for (int i = startIndex; i != endIndex; i = (i + 1) % Pieces.Count)
        {
            result.Add(Pieces[i]);
        }
        return result;
    }

    //Assumption: startPiece must be the switch piece
    public int GetShortestSwitchSegment(int indexOfStartPiece, int currLaneIndex)
    {
        int nextIndex = (indexOfStartPiece + 1) % Pieces.Count;
        //Distance of the different lanes
        double[] distance;
        bool hasSwitch = false;
        //Index of possible lanes
        int[] possibleLanes;
        if (currLaneIndex > 0 && currLaneIndex < Lanes.Count - 1)
        {
            possibleLanes = new int[3] { currLaneIndex - 1, currLaneIndex, currLaneIndex + 1 };
            distance = new double[3] { 0, 0, 0 };
        }
        else if (currLaneIndex == 0)
        {
            possibleLanes = new int[2] { currLaneIndex, currLaneIndex + 1 };
            distance = new double[2] { 0, 0 };
        }
        else
        {
            possibleLanes = new int[2] { currLaneIndex - 1, currLaneIndex };
            distance = new double[2] { 0, 0 };
        }

        while (hasSwitch == false)
        {
            //Adds the lane lengths
            for (int i = 0; i < possibleLanes.Length; i++)
            {
                int currLane = possibleLanes[i];
                double sectionLength = Pieces[nextIndex].CalcLength(Lanes[currLane].DistanceFromCenter);
                distance[i] += sectionLength;
                //if (sectionLength < 0)
                //{
                //    Console.WriteLine("Here is the mistake");
                //    Console.WriteLine(Pieces[nextIndex].Name + " length " + sectionLength);
                //    Console.WriteLine("Lane offset" + Lanes[currLane].DistanceFromCenter);
                //}
            }
            //stops when switch is encountered
            nextIndex = (nextIndex + 1) % Pieces.Count;
            hasSwitch = Pieces[nextIndex].HasSwitch;
        }
        //Finds the shortest lane length
        int shortestSegmentLaneIndex = currLaneIndex;
        double shortest = Double.MaxValue;
        double currLaneDistance;
        if (currLaneIndex == 0)
        {
            currLaneDistance = distance[0];
        }
        else
        {
            currLaneDistance = distance[1];
        }

        for (int i = 0; i < distance.Length; i++)
        {
            if (shortest > distance[i])
            {
                shortest = distance[i];
                shortestSegmentLaneIndex = possibleLanes[i];
            }
            //Console.WriteLine("Lane index: " + possibleLanes[i] + " is length: " + distance[i]);
        }
        //Return currentLaneIndex if they are the same length
        if (shortest == currLaneDistance)
        {
            return currLaneIndex;
        }
        //else return the index of the shortest segment
        return shortestSegmentLaneIndex;
    }

    //Returns the distance between two cars, assuming that they are in the same lane
    public double GetDistanceInLane(CarPosition from, CarPosition to)
    {


        return GetDistanceInLane(from.PiecePosition, to.PiecePosition);
    }

    public double GetDistanceInLane(PiecePosition from, PiecePosition to)
    {

        //From lane index and offset
        int fromLaneIndex = from.lane.EndLaneIndex;
        double fromLaneOffset = Lanes[fromLaneIndex].DistanceFromCenter;

        //To lane index and offset
        int toLaneIndex = from.lane.EndLaneIndex;
        double toLaneOffset = Lanes[toLaneIndex].DistanceFromCenter;

        //If not on same lane
        if (fromLaneIndex != toLaneIndex)
        {
            Console.WriteLine("ERROR getDistanceInLane called on two cars, which are not in same lane");
            return 0;
        }

        //Piece indexes
        int startPieceIndex = from.PieceIndex;
        int endPieceIndex = to.PieceIndex;

        //Distance left of start piece
        double startPieceDistanceLeft = Pieces[startPieceIndex].CalcLength(fromLaneOffset) - from.InPieceDistance;
        //Distance traveled on end piece
        double endPieceDistanceTravelled = to.InPieceDistance;

        //If from and to is on same track piece
        if (startPieceIndex == endPieceIndex)
        {
            return to.InPieceDistance - from.InPieceDistance;
        }
        //else on different track pieces
        double distance = 0;
        int index = (startPieceIndex + 1) % Pieces.Count;

        while (index != endPieceIndex && index != startPieceIndex)
        {
            distance += Pieces[index].CalcLength(fromLaneOffset);
            index = (index + 1) % Pieces.Count;

        }
        //Plus start piece distance left to cover
        distance += startPieceDistanceLeft;
        //Plus end piece distance covered
        distance += endPieceDistanceTravelled;
        //Checks if a full circuit has been made;
        if (index == startPieceIndex)
        {
            Console.WriteLine("ERROR: A full circuit has been made in getDistanceInLane");
        }
        return distance;
    }
    //returns a list of Trackpieces, which contains the pieces of the next curve. If the car is currently
    //cornering the list of pieces will be the next curve the car needs to handle. If 
    //the car already is in the only curve on the track, it will return an empty list;
    //It will differentiate between radius of the bend. 
    public List<Piece> GetNextCurve(Piece carPos)
    {
        int currIndex = Pieces.IndexOf(carPos);

        //Moves the index out of the current curve
        currIndex = GetCurveEnd(carPos);

        //Finds the beginning og the next curve;
        while (Pieces[currIndex].GetType() == typeof(Straight))
        {
            currIndex = (currIndex + 1) % Pieces.Count;
        }
        //The limits of the curve we want to return
        int curveStartIndex = currIndex;
        int curveEndIndex = GetCurveEnd(Pieces[currIndex]);
        List<Piece> theCurve = new List<Piece>();
        for (int i = curveStartIndex; i != curveEndIndex; i = (i + 1) % Pieces.Count)
        {
            //Console.WriteLine("Piece of next curve: " + Pieces[i].Name);
            theCurve.Add(Pieces[i]);
        }
        //add the final piece
        theCurve.Add(Pieces[curveEndIndex]);
        return theCurve;

    }
    //Returns the first piece after this curve. 
    //If the input is not a curve the input index is returned
    //If the entire track is the same curve, the input is returned
    //If a curve changes angle or radius, it is considered a new curve
    public int GetCurveEnd(Piece piece)
    {

        int currIndex = Pieces.IndexOf(piece);
        double startCurveRadius;
        bool startCurveRight;
        int startIndex = currIndex;

        if (piece.GetType() == typeof(Curve))
        {
            Curve curvePiece = piece as Curve;
            startCurveRadius = curvePiece.Radius;
            startCurveRight = curvePiece.Angle > 0;
            //Skips the current curve, if in a curve
            while (Pieces[currIndex].GetType() == typeof(Curve))
            {

                Curve currPiece = Pieces[currIndex] as Curve;

                //If new curve
                if ((currPiece.Angle > 0) != startCurveRight || currPiece.Radius != startCurveRadius)
                {
                    break;
                }

                //Update counter
                currIndex = (currIndex + 1) % Pieces.Count;

                //Entire track is a curve
                if (currIndex == startIndex)
                {
                    return currIndex;
                }
            }
        }
        return currIndex;
    }

    public string Id { get { return this._id; } }
    public string Name { get { return this._name; } }
    public List<Piece> Pieces { get { return this._pieces; } }
    public List<Lane> Lanes { get { return this._lanes; } }
    public List<TrackSegment> SwitchSegments { get { return this._switchSegments; } }
}



//Matches the lanes part of the track part og the gameInit message
class Lane
{
    private int _distanceFromCenter;
    private int _index;

    public Lane(int distanceFromCenter, int index)
    {
        this._distanceFromCenter = distanceFromCenter;
        this._index = index;
    }

    public int DistanceFromCenter { get { return this._distanceFromCenter; } }
    public int Index { get { return this._index; } }
}

//Matches the data of both types of pieces
class PieceData
{
    private double _length;
    private bool _hasSwitch;

    PieceData(double length)
        : this(length, false)
    {
    }
    PieceData(double length, bool @switch)
    {
        this._length = length;
        this._hasSwitch = @switch;
    }

    public double Length { get { return _length; } }
    public bool HasSwitch { get { return _hasSwitch; } }

    private double _radius;
    private double _angle;
    [JsonConstructor]
    PieceData(double radius, double angle, double length, bool @switch)
    {
        this._radius = radius;
        this._angle = angle;
        this._length = length;
        this._hasSwitch = @switch;
    }

    public double Radius { get { return this._radius; } }
    public double Angle { get { return this._angle; } }


}

//Abstract class over piece types
abstract class Piece
{
    protected bool _hasSwitch = false;
    private string _name;

    public Piece(string name)
    {
        this._name = name;
    }

    public abstract double CalcMaxSpeed(double maxCtpForce, double laneOffset);
    public abstract double CalcLength(double laneOffset);
    public bool HasSwitch { get { return this._hasSwitch; } }
    public string Name { get { return _name; } }
}
//Actual straight track piece
class Straight : Piece
{
    private double _length;


    Straight(double length)
        : this(length, false)
    {
    }
    public Straight(double length, bool @switch)
        : base("Straight Length: " + length + " Switch: " + @switch)
    {
        this._length = length;
        base._hasSwitch = @switch;

    }

    public override double CalcLength(double laneOffset)
    {
        return this.Length;
    }
    public override double CalcMaxSpeed(double maxCtpForce, double laneOffset)
    {
        return double.MaxValue;
    }
    public double Length { get { return _length; } }


}
//Actual curved track piece
class Curve : Piece
{
    private double _radius;
    private double _angle;

    public Curve(double radius, double angle, bool @switch)
        : base("Curve Angle: " + angle + " Radius: " + radius + " Switch: " + @switch)
    {
        this._radius = radius;
        this._angle = angle;
        base._hasSwitch = @switch;

    }

    public override double CalcLength(double laneOffset)
    {
        double diameter = AdjustedRadius(laneOffset) * 2;
        return diameter * Math.PI * Math.Abs(Angle) / 360;  //Diameter times pi times fraction of circle
    }

    public double AdjustedRadius(double laneOffset)
    {
        double radius;
        //IF turning right - positive angle, then negate offset
        if (Angle > 0)
        {
            radius = Radius - laneOffset;
        }
        else
        {
            //Else turning left - negative angle, then add offset
            radius = Radius + laneOffset;
        }
        return radius;
    }

    public override double CalcMaxSpeed(double maxCtpForce, double laneOffset)
    {
        return Math.Sqrt(maxCtpForce * AdjustedRadius(laneOffset));
    }
    public double Radius { get { return this._radius; } }
    public double Angle { get { return this._angle; } }

}

class MsgWrapper
{
    public string msgType;
    public Object data;

    public MsgWrapper(string msgType, Object data)
    {
        this.msgType = msgType;
        this.data = data;
    }
}

class TickMsgWrapper : MsgWrapper
{
    public int gameTick;

    public TickMsgWrapper(string msgType, Object data, int tick)
        : base(msgType, data)
    {
        this.gameTick = tick;
    }
}



abstract class SendMsg
{
    public virtual string ToJson()
    {
        return JsonConvert.SerializeObject(new MsgWrapper(this.MsgType(), this.MsgData()), new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
    }
    protected virtual Object MsgData()
    {
        return this;
    }

    protected abstract string MsgType();
}

abstract class SendMsgTick : SendMsg
{
    public override string ToJson()
    {
        return JsonConvert.SerializeObject(new TickMsgWrapper(this.MsgType(), this.MsgData(), this.Tick()), new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
    }

    protected abstract int Tick();
}

class Join : SendMsg
{
    public string name;
    public string key;

    public Join(string name, string key)
    {
        this.name = name;
        this.key = key;
    }

    protected override string MsgType()
    {
        return "join";
    }
}

class JoinRace : SendMsg
{
    public class BotId
    {
        public string name;
        public string key;
    }
    public BotId botId = new BotId();
    public string trackName;
    public string password;
    public int carCount;

    public JoinRace(string name, string key, string trackName, string password, int carCount)
    {
        this.botId.name = name;
        this.botId.key = key;
        this.trackName = trackName;
        if (password != null)
        {
            this.password = password;
        }
        else
        {
            this.password = password;
        }
        this.carCount = carCount;
    }

    protected override string MsgType()
    {
        return "joinRace";
    }


}

class Ping : SendMsg
{
    protected override string MsgType()
    {
        return "ping";
    }
}

class Throttle : SendMsg
{
    public double value;

    public Throttle(double value)
    {
        this.value = value;
    }

    protected override Object MsgData()
    {
        return this.value;
    }

    protected override string MsgType()
    {
        return "throttle";
    }
}

class TurboTick : SendMsgTick
{
    public string data;
    public int tick;

    public TurboTick(string data, int tick)
    {
        this.data = data;
        this.tick = tick;
    }

    protected override Object MsgData()
    {
        return this.data;
    }

    protected override int Tick()
    {
        return this.tick;
    }
    protected override string MsgType()
    {
        return "turbo";
    }
}

class ThrottleTick : SendMsgTick
{
    public double value;
    public int tick;

    public ThrottleTick(double value, int tick)
    {
        this.value = value;
        this.tick = tick;
    }

    protected override Object MsgData()
    {
        return this.value;
    }

    protected override int Tick()
    {
        return this.tick;
    }

    protected override string MsgType()
    {
        return "throttle";
    }
}

class SwitchLaneTick : SendMsgTick
{
    public string value;
    public int tick;

    public SwitchLaneTick(string value, int tick)
    {
        this.value = value;
        this.tick = tick;
    }
    protected override Object MsgData()
    {
        return this.value;
    }

    protected override string MsgType()
    {
        return "switchLane";
    }

    protected override int Tick()
    {
        return this.tick;
    }
}

class SwitchLane : SendMsg
{
    public string value;

    public SwitchLane(string value)
    {
        this.value = value;
    }
    protected override Object MsgData()
    {
        return this.value;
    }

    protected override string MsgType()
    {
        return "switchLane";
    }
}

